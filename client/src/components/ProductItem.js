import React, {useEffect, useState} from 'react';
import Col from "react-bootstrap/Col";
import {Card, Image} from "react-bootstrap";
import star from '../asstets/star.png'
import {useHistory} from "react-router-dom";
import {PRODUCT_ROUTE} from "../utils/const";
import {observer} from "mobx-react-lite";
import {getRating} from "../http/deviceAPI";

const ProductItem = ({device}) => {
    const [rate, setRate] = useState(5)
    const history = useHistory()

    useEffect(() => {
        getRating(device.id).then(data => {

            if(data.data.length>0)
              setRate(data.data.reduce((a,b) => a+b.rate , 0)/data.data.length)

        })

    }, [rate])

    return (
        <Col md={3} className="mt-3" onClick={() => history.push(PRODUCT_ROUTE+'/'+device.id)}>
            <Card style={{width: 150, cursor: 'pointer'}} border={"light"}>
                <Image width={150} height={150} src={device.image}/>
                <div className="text-black-50 mt-1 d-flex justify-content-between align-items-center">

                    <div className="d-flex align-items-center">
                        <div>{rate.toFixed(1)}</div>
                        <Image width={20} height={20} src={star}/>
                    </div>

                </div>
                <div>{device.title}</div>
            </Card>
        </Col>

    );
};

export default observer(ProductItem);