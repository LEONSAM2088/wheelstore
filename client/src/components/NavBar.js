import React, {useContext} from 'react';
import {Context} from "../index";
import {Button, Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {useHistory} from "react-router-dom";
import {ADDPRODUCT_ROUTE, BASKET_ROUTE, LOGIN_ROUTE, ORDERS_ROUTE, PROFILE_ROUTE, SHOP_ROUTE} from "../utils/const";

const NavBar = observer(() => {
    const {user} = useContext(Context)
    const history = useHistory()
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand onClick={() => history.push(SHOP_ROUTE)} style={{cursor: "pointer"}}>WheelStore</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">

                        {
                            user.LevelAccess===0? <Nav.Link onClick={() => history.push(ORDERS_ROUTE)}>Заказы</Nav.Link>:
                                <Nav.Link onClick={() => history.push(ORDERS_ROUTE)}>Мои заказы</Nav.Link>
                        }

                        <Nav.Link onClick={() => history.push(BASKET_ROUTE)}>Корзина</Nav.Link>
                        {
                            user.LevelAccess===0 &&  <Nav.Link onClick={() => history.push(ADDPRODUCT_ROUTE)}>Добавить товар</Nav.Link>
                        }


                    </Nav>
                     {!user.isAuth || user.LevelAccess===2?
                    <Nav>
                        <Button variant={"outline-light"} onClick={()=>history.push(LOGIN_ROUTE)}  > Авторизация </Button>

                    </Nav> :
                         <Nav>
                             <Button variant={"outline-light"} onClick={() => history.push(PROFILE_ROUTE)}> Профиль </Button>

                         </Nav>
                     }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
});

export default NavBar;