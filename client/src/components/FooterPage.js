import React, {useContext} from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import {Button, Nav} from "react-bootstrap";
import {ADDPRODUCT_ROUTE, BASKET_ROUTE, LOGIN_ROUTE, ORDERS_ROUTE, PROFILE_ROUTE} from "../utils/const";
import {Context} from "../index";
import {useHistory} from "react-router-dom";

const FooterPage = () => {
    return (
        <MDBFooter color="blue" className="font-small pt-4 mt-4">
            <MDBContainer fluid className="text-center text-md-left">
                <MDBRow>
                    <MDBCol md="6">
                        <h5 className="title">WheelStore</h5>
                        <p>
                            Едь к своей мечте
                        </p>
                    </MDBCol>

                </MDBRow>
            </MDBContainer>
            <div className="footer-copyright text-center py-3">
                <MDBContainer fluid>
                    &copy; {new Date().getFullYear()} Copyright: <a href="https://localhost:3000"> WheelStore.com </a>
                </MDBContainer>
            </div>
        </MDBFooter>
    );
}

export default FooterPage;