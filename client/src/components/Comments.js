import React, {useContext} from 'react';
import {Button, Card, Container, FormLabel, Image, Row} from "react-bootstrap";
import star from "../asstets/star.png";
import {Context} from "../index";
import Col from "react-bootstrap/Col";
import {observer} from "mobx-react-lite";

const Comments = ({item}) => {
    console.log(item.rate)

    window.scrollTo(0, 0)

    return (
        <Container>
            <Card style={{height: 500, marginBottom: 20}}>
                <Row className="mt-2">



                   <div style={{width: 200, display: "flex", position: "absolute", marginLeft: 20}} >
                       <img style={{borderRadius: 100}}  width={100} height={100} src={

                           item.user_image?item.user_image:'https://sun9-22.userapi.com/impg/zvYRDTJe3v51trKIOhHfJVKU3rBn_lajb0I2WA/SjX1VArVZYA.jpg?size=1280x823&quality=96&sign=d009c32b754cd5f2d774bfe9070b5ae8&type=album'
                       } alt={"smth"}/>
                       <div style={{width: 100}}>{item.user_name}</div>
                   </div>
                </Row>
                <div style={{position: "absolute", right: 20, top: 20}}>
                    <div className="d-flex align-items-center" style={{width: 50, height: 20, backgroundColor: "#0d6efd",color: "white", padding: 15, borderRadius: 2, borderType: "solid"}}>
                        <div>{item.rate}</div>
                        <Image width={20} height={20} src={star}/>
                    </div>
                </div>
                <div style={{padding: 9, position: "absolute", top: 100, left: 200, display: "block", width: 800, overflow: "hidden", borderStyle: "solid"}}>
                    <p>
                        {item.comment}
                    </p>
                </div>


            </Card>
        </Container>
    );
};

export default observer(Comments);