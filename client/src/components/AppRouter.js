import React, {useContext} from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import {authRoutes, fullAuthRoutes, publicRoutes} from "../routes";
import Shop from "../pages/Shop";
import {Context} from "../index";
import {observer} from "mobx-react-lite";

const AppRouter = () => {
    const {user} = useContext(Context);



    return (
        <Switch>
            {(user.LevelAccess>0 && user.LevelAccess<2 || user.isAuth) && authRoutes.map(({path, Component}) =>
                <Route key={path} path={path} component={Component} exact/>
            )}

            {user.LevelAccess===0 && fullAuthRoutes.map(({path, Component}) =>
                <Route key={path} path={path} component={Component} exact/>
            )}





            {publicRoutes.map(({path, Component}) =>
                <Route key={path} path={path} component={Component} exact/>
            )}
            <Redirect to={Shop}/>
        </Switch>
    );
};

export default observer(AppRouter);