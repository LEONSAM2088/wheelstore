import React, {useContext, useEffect} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Row} from "react-bootstrap";
import ProductItem from "./ProductItem";

const DeviceList = () => {
    const {device} = useContext(Context)

    return (
        <Row className={"d-flex"}>
            {device.products.map(device =>
                <ProductItem key={device.id} device={device}/>
            )}
        </Row>
    );
};

export default observer(DeviceList);