import React, {useContext, useEffect, useState} from 'react';
import {BrowserRouter, useParams} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import NavBar from "./components/NavBar";
import FooterPage from "./components/FooterPage";
import {observer} from "mobx-react-lite";
import {Context} from "./index";
import {check, getUserInfo, registrationTempUser} from "./http/userAPI";
import {set} from "mobx";
import {Spinner} from "react-bootstrap";
import {getCommentsForProduct, getProducts} from "./http/deviceAPI";
import {getAllOrders, getBasket, getMyOrders} from "./http/basketApi";

const App = () => {
    const {user} = useContext(Context)
    const [loading, setLoading] = useState(true)
    const {device} = useContext(Context)

    const {order} = useContext(Context)


    useEffect(() => {

        if(localStorage.getItem("token"))
            check().then(data => {
                getUserInfo().then((data) => {

                      user.setUser(data)
                      user.setLevelAccess(data.role)
                      user.setIsAuth(true)

                    if(user.user.role===0) {

                        getAllOrders().then(data => {

                            order.setOrders(data)


                        }).finally(() => setLoading(false)) }
                    else
                        getMyOrders().then(data => {

                            order.setOrders(data)

                        }).finally(() => setLoading(false))
                })




            }).finally(() => setLoading(false))
                else
            registrationTempUser().finally(() => setLoading(false))

        getProducts().then(data => {
            device.setDevices(data)
        }).finally(() => setLoading(false))



    }, [device, user])
    if(loading) {
        return <Spinner animation={"grow"}/>
    }

    return (
        <BrowserRouter>
            <NavBar />
          <AppRouter />
            <FooterPage/>
        </BrowserRouter>
    );
};

export default observer(App);