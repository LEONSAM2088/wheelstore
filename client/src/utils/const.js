export const ADMIN_ROUTE = '/admin'
export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const SHOP_ROUTE = '/'
export const BASKET_ROUTE = '/basket'
export const PRODUCT_ROUTE = '/product'
export const ORDERS_ROUTE = '/MyOrders'
export const COMMENTS_ROUTE = '/Comments'
export const PROFILE_ROUTE = '/profile'
export const CHECKOUT_ROUTE = '/checkout'
export const ADDCOMMENT_ROUTE = '/addComment'
export const ADDPRODUCT_ROUTE = '/addProduct'