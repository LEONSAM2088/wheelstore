import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode"

export const pushProduct = async (id) => {
    const {data} = await $authHost.post('api/v1/basket', {product_id: id})

    return data
}

export const getBasket = async () => {
    const {data} = await $authHost.get('api/v1/basket')

    return data
}


export const checkOut = async (deliveryAddress) => {
    const {data} = await $authHost.post('api/v1/orders', {delivery_address: deliveryAddress})

    return data
}

export const dropProductFormBasket = async (id) => {
    const {data} = await $authHost.delete('api/v1/basket/'+id )

    return data
}

export const getMyOrders = async () => {
    const {data} = await $authHost.get('api/v1/orders')

    return data
}
export const getAllOrders = async () => {
    const {data} = await $authHost.get('api/v1/orders/all')

    return data
}

export const dropOrder = async (id) => {
    const {data} = await $authHost.delete('api/v1/order/'+id)

    return data
}