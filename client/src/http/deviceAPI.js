import {$authHost, $host} from "./index";

export const getProducts = async () => {
    const {data} = await $host.get('api/v1/products')

    return data
}

export const getProduct = async (id) => {
    const {data} = await $host.get('api/v1/products/'+id)

    return data
}

export const createComment = async (id ,rate, text) => {
    const {data} = await $authHost.post('api/v1/comments/'+id, {text})
    const {data2} = await $authHost.post('api/v1/rating/'+id, {rate})


    return {data, data2}
}

export const getCommentsForProduct = async (id) => {
    const {data} = await $authHost.get('api/v1/comments/'+id)



    return data
}

export const getRating = async (id) => {
    return await $authHost.get('api/v1/rating/' + id)
}

export const createProduct = async (title, price) => {


    const response =  $authHost.post('api/v1/products', {title, price}).then().catch(err => console.log(err))

    return response
}
export const editProduct = async (id, title, price) => {


    const response = $authHost.put('api/v1/products/' + id, {title, price}).then().catch(err => console.log(err))
console.log(response)
    return response}

export const setImageProduct = async (id, image) => {
    const formData = new FormData();
    formData.append('',image); // appending file
    $authHost.put('api/v1/products/'+id, formData).then().catch(err => console.log(err))

}


