import axios from "axios";

const $host = axios.create({
    baseURL: process.env.REACT_APP_API_URL
})

const $authHost = axios.create({
    withCredentials: true,
    baseURL: process.env.REACT_APP_API_URL
})

const authInterceptor = config => {
    config.headers.authorization = `Bearer ${localStorage.getItem('token')}`
    return config
}

const authResInterceptor = config => {
    return config
}

$authHost.interceptors.request.use(authInterceptor)
$authHost.interceptors.response.use(authInterceptor, async(error)   =>  {
    if(error.response.status === 401 && error.config && !error.config._isActive) {
        error.config._isActive = true
        try {
            const response = await $authHost.get('api/v1/auth/token/refresh')
            localStorage.setItem("token", response.data.access)
            return $authHost.request(error.config)
        } catch (e) {
            console.log("Не авторизован")
        }
    }
    throw error
})

export  {
    $host,
    $authHost
}