import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode"

export const registration = async (name, email, password) => {
    const {data} = await $authHost.post('api/v1/auth/register', {name, email, password, role: 0})

    return data
}

export const registrationTempUser = async () => {


    const {data} = await $host.post('api/v1/auth/register', {name:"tmpUser", email:""+Date.now(), password:"", role: 2})

    //if(!localStorage.getItem('token'))
    localStorage.setItem('token',data.access)

    return data


}

export const login = async (email, password) => {
    const {data} = await $authHost.post('api/v1/auth/login', {email, password})
    localStorage.setItem('token',data.user.access)
    //await $authHost.get('api/v1/auth/token/refresh', {withCredentials: true})

    return data.user
}

//export const check = async () => {

    //await refreshToken()
   // console.log(response)

    //localStorage.setItem("token", response.data.access)
    //return jwt_decode(response.da).sub
//}
export const check = async () => {
    const {data} = await $authHost.get('api/v1/auth/check')
    //document.cookie = "refresh_token_cookie=";
    //localStorage.setItem("token", data.access)
    return data
}
export const getUserInfo = async () => {
    const {data} = await $authHost.get('api/v1/auth/me')

    return data
}
export const refreshToken = async () => {
    const response = await $authHost.get('api/v1/auth/token/refresh')
    localStorage.setItem('token', response.data.access)
    return response
}
export const setUserImage = async (id, file) => {
    const formData = new FormData();
    formData.append('',file); // appending file
    const response = $authHost.put('api/v1/users/'+id, formData).then().catch(err => console.log(err))
    console.log(id+" "+ file )

    return response
//     const {data} = await $authHost.put('api/v1/users/' + id, file, {
//         headers: {
//         'Content-Type': 'multipart/form-data'
//     }
// })

}