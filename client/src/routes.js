import Admin from "./pages/Admin"
import {
    ADDCOMMENT_ROUTE, ADDPRODUCT_ROUTE,
    ADMIN_ROUTE,
    BASKET_ROUTE, CHECKOUT_ROUTE, COMMENTS_ROUTE,
    LOGIN_ROUTE,
    ORDERS_ROUTE,
    PRODUCT_ROUTE, PROFILE_ROUTE,
    REGISTRATION_ROUTE,
    SHOP_ROUTE
} from "./utils/const";
import Shop from "./pages/Shop";
import ProductPage from "./pages/ProductPage";
import Auth from "./pages/Auth";
import Basket from "./pages/Basket";
import Orders from "./pages/Orders";
import CommentsPage from "./pages/CommentsPage";
import ProfilePage from "./pages/ProfilePage"
import CheckOut from "./pages/CheckOut";
import AddCommentPage from "./pages/AddCommentPage";
import AddProductPage from "./pages/AddProductPage";

export const fullAuthRoutes = [
    {
        path: ADMIN_ROUTE,
        Component: Admin
    },

    {
        path: ADDPRODUCT_ROUTE,
        Component: AddProductPage
    },

]

export const authRoutes = [

    {
        path: ADDCOMMENT_ROUTE+'/:id',
        Component: AddCommentPage
    },

    {
        path: PROFILE_ROUTE,
        Component: ProfilePage
    },
]

export const publicRoutes = [
    {
        path: SHOP_ROUTE,
        Component: Shop
    },
    {
        path: PRODUCT_ROUTE + '/:id',
        Component: ProductPage
    },
    {
        path: REGISTRATION_ROUTE,
        Component: Auth
    },
    {
        path: LOGIN_ROUTE,
        Component: Auth
    }
    ,
    {
        path: BASKET_ROUTE,
        Component: Basket
    },
    {
        path: ORDERS_ROUTE,
        Component: Orders
    },
    {
        path: COMMENTS_ROUTE+ '/:id',
        Component: CommentsPage
    },

    {
        path: CHECKOUT_ROUTE,
        Component: CheckOut
    },
]