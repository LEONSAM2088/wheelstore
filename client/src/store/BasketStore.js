import {makeAutoObservable} from "mobx";

export default class BasketStore {
    get products() {
        return this._products;
    }

     setProducts(value) {
        this._products = value;
    }
    constructor() {
        this._products = []
        makeAutoObservable(this)
    }

}
