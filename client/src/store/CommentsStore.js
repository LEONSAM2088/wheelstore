import {makeAutoObservable} from "mobx";

export default class CommentsStore {
    get comments() {
        return this._comments;
    }

    setComments(value) {
        this._comments = value;
    }

    constructor() {
        this._comments = [
        ]
        makeAutoObservable(this)
    }

}
