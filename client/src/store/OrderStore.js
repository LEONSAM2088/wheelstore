import {makeAutoObservable} from "mobx";

export default class OrderStore {
    get orders() {
        return this._orders;
    }

    setOrders(value) {
        this._orders = value;
    }



    constructor() {
        this._orders = []

        makeAutoObservable(this)
    }

}
