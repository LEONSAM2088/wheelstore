import {makeAutoObservable} from "mobx";

export default class UserStore {


    constructor() {
        this._isAuth = false
        this._levelAccess = 2
        this._user = {}

        makeAutoObservable(this)
    }


    get isAuth() {
        return this._isAuth;
    }

    setIsAuth(value) {
        this._isAuth = value;
    }

    setLevelAccess(level) {
        this._levelAccess = level
    }
    setUser(user) {
        this._user = user
    }
    get LevelAccess() {
        return this._levelAccess
    }
    get user() {
        return this._user
    }

}
