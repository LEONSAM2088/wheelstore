import React, {useContext, useEffect, useState} from 'react';
import {Button, Card, Container, Form, Row} from "react-bootstrap";
import {NavLink, useHistory, useLocation} from "react-router-dom"
import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/const";
import {login, registration} from "../http/userAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {getBasket, getMyOrders} from "../http/basketApi";

const Auth = () => {
    const {user} = useContext(Context)
    const {basket} = useContext(Context)
    const {order} = useContext(Context)
    const location = useLocation()
    const isLogin = location.pathname === LOGIN_ROUTE
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const history = useHistory()

    const click = async () => {
        let data;
        if (isLogin) {
           await login(email, password).then((data)=>{


                user.setUser(data.user)
                user.setLevelAccess(data.user.role)
                user.setIsAuth(true)

                getMyOrders().then(data => {

                    order.setOrders(data)

                }).finally()
                getBasket().then(r=> {

                    basket.setProducts(r)
                })
                history.push(SHOP_ROUTE)
            }).catch(()=> alert("Неправидьные логин и пароль"))

        } else {
            await registration(name, email, password)
            alert("Пользователь зарегистрирован")
            history.push(LOGIN_ROUTE)
        }


    }
    return (
        <Container className={"d-flex justify-content-center align-items-center"}
        style={{height: window.innerHeight - 54}}
        >
            <Card style={{width: 600}} className="p-5">
                <h2 className="m-auto">{isLogin?'Авторизация':'Регистрация'}</h2>
                <Form className="d-flex flex-column">
                    {!isLogin && <Form.Control
                        className="mt-3"
                        placeholder="Введите имя..."
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />}
                    <Form.Control
                        className="mt-3"
                        placeholder="Введите почту..."
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />

                    <Form.Control
                        className="mt-3"
                        placeholder="Введите пароль..."
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type={"password"}
                    />
                    <Row className="d-flex justify-content-between mt-3 pl-3 pr-3">
                        {isLogin?
                            <div>
                                Нет аккаунта? <NavLink to={REGISTRATION_ROUTE}>Лох получается!</NavLink>
                            </div>
                            :
                            <div>
                                Есть аккаунт? <NavLink to={LOGIN_ROUTE}>Не лох получается!</NavLink>
                            </div>
                        }
                        <Button
                            className="align-self-end"
                            variant="outline-success"
                            onClick={click}
                        >

                            {isLogin?'Войти':'Регистрация'}
                        </Button>
                    </Row>

                </Form>
            </Card>


        </Container>
    );
};

export default observer(Auth);