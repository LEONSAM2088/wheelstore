import React, {useContext, useState} from 'react';
import {Button, Card, Container, Form, Image, Row} from "react-bootstrap";
import Comments from "../components/Comments";
import star from "../asstets/star.png";
import {Context} from "../index";
import '../style.css';

import {observer} from "mobx-react-lite";
import {createComment} from "../http/deviceAPI";
import {useHistory, useParams} from "react-router-dom";
import {SHOP_ROUTE} from "../utils/const";
const AddCommentPage = () => {

    const history = useHistory()
    const [rating, setRating] = useState(0)
    const [comments, setComments] = useState("")
    const {device} = useContext(Context)
    const {user} = useContext(Context)
    const {id} = useParams()
console.log(user)

    return (
        <Container>
            <Container>
            <Card style={{height: 500, marginBottom: 20, width: "100%",}}>
                <Row className="mt-2">



                    <div style={{width: 200, display: "flex",  marginLeft: 20}} >
                        <img style={{borderRadius: 100}} src={user.user.image?user.user.image:'https://sun9-22.userapi.com/impg/zvYRDTJe3v51trKIOhHfJVKU3rBn_lajb0I2WA/SjX1VArVZYA.jpg?size=1280x823&quality=96&sign=d009c32b754cd5f2d774bfe9070b5ae8&type=album'} width={100} height={100} alt={"smth"}/>
                        <div style={{width: 100}}>{user.user.name}</div>
                    </div>
                </Row>
                <div style={{position: "absolute", right: 20, top: 20}}>
                       <div className="rating-area">
                            <input type="radio" id="star-5" name="rating" value="5" onChange={() => setRating(5)}/>
                            <label htmlFor="star-5" title="Оценка «5»"></label>
                            <input type="radio" id="star-4" name="rating" value="4" onChange={() => setRating(4)}/>
                            <label htmlFor="star-4" title="Оценка «4»"></label>
                            <input type="radio" id="star-3" name="rating" value="3" onChange={() => setRating(3)}/>
                            <label htmlFor="star-3" title="Оценка «3»"></label>
                            <input type="radio" id="star-2" name="rating" value="2" onChange={() => setRating(2)}/>
                            <label htmlFor="star-2" title="Оценка «2»"></label>
                            <input type="radio" id="star-1" name="rating" value="1" onChange={() => setRating(1)}/>
                            <label htmlFor="star-1" title="Оценка «1»"></label>
                        </div>
                    </div>

                <div style={{margin:"0 auto",padding: 9, top: 200, left: "10%", display: "block", width: 800, overflow: "hidden", borderStyle: "solid"}}>


                    <Form>

                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">

                            <Form.Control as="textarea" rows={3} onChange={(e) => setComments(e.target.value)}/>
                        </Form.Group>
                        <Button variant={"outline-success"} onClick={() => createComment(id, rating, comments).then(() => history.push(SHOP_ROUTE))}>Сохранить</Button>
                        <Button variant={"outline-danger"}>Отменить</Button>
                    </Form>
                </div>


            </Card>
            </Container>
        </Container>
    );
};

export default observer(AddCommentPage);