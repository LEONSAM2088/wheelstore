import React, {useState} from 'react';
import {Button, Card, Container, Form, FormControl, FormLabel, InputGroup, Row} from "react-bootstrap";
import {NavLink, useHistory} from "react-router-dom";
import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/const";
import {createProduct, editProduct, setImageProduct} from "../http/deviceAPI";

const AddProductPage = () => {

    const history = useHistory()
    const isEdit = !!history.location.state
     if(history.location.state)
        console.log(history.location.state.id)
    const [title, setTitle] = useState(isEdit && history.location.state.title);
    const [file, setFile] = useState();
    const [price, setPrice] = useState(isEdit && history.location.state.price);
    const save = () => {
        createProduct(title, price).then((data) => {
            setImageProduct(data.data.product.id, file).then(r => history.push(SHOP_ROUTE))

        })
    }
    const edit = () => {

        editProduct(history.location.state.id, title, price).then((data) => {

            if(file)
                setImageProduct(data.data.product.id, file).then(r => history.push(SHOP_ROUTE))

        }).then(r => history.push(SHOP_ROUTE))
    }
    return (
        <Container className={"d-flex justify-content-center align-items-center"}
                   style={{height: window.innerHeight - 54}}
        >
            <Card style={{width: 600}} className="p-5">
                <InputGroup className="mb-3">
                    <InputGroup.Text>Название товара</InputGroup.Text>
                    <FormControl aria-label="Name" value={title?title:''} onChange={(e)=>setTitle(e.target.value)}/>
                </InputGroup>
                <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>Загрузить изображение товара</Form.Label>
                    <Form.Control type="file" onChange={(e)=>setFile(e.target.files[0])}/>
                </Form.Group>
                <FormLabel>Цена товара</FormLabel>
                <InputGroup className="mb-3">
                    <InputGroup.Text>$</InputGroup.Text>
                    <FormControl aria-label="Amount (to the nearest dollar)" value={price?price:''} onChange={(e)=>setPrice(e.target.value)}/>
                    <InputGroup.Text>.00</InputGroup.Text>
                </InputGroup>
                {!isEdit?<Button variant={"outline-success"} onClick={()=>save()}>Сохранить</Button>:
                    <Button variant={"outline-success"} onClick={()=>edit()}>Изменить</Button>}

                <Button variant={"outline-danger"} onClick={()=>history.push(SHOP_ROUTE)}>Отменить</Button>
            </Card>


        </Container>

    );
};

export default AddProductPage;