import React, {useContext, useEffect, useState} from 'react';
import {Accordion, Button, Card, Container, Row} from "react-bootstrap";
import {CHECKOUT_ROUTE, PROFILE_ROUTE, SHOP_ROUTE} from "../utils/const";
import ProductItem from "../components/ProductItem";
import {Context} from "../index";
import Col from "react-bootstrap/Col";
import DeviceList from "../components/DeviceList";
import {useHistory} from "react-router-dom";
import {observer} from "mobx-react-lite";
import {dropProductFormBasket, getBasket} from "../http/basketApi";

const Basket = () => {
   const {device} = useContext(Context)
    const {basket} = useContext(Context)
    const history = useHistory()

    const removeFromBasket = (id) => {
        basket.setProducts(basket.products.filter( i => id!==i.basket_id))
        dropProductFormBasket(id).then()
    }

    useEffect(() => {

        getBasket().then(r=> {

            basket.setProducts(r)
        })

        }, [])

    return (

        <Container style={{padding: 40}}>
            <h1>Корзина</h1>

            {

                basket.products.length===0?
            <Card className={" justify-content-center align-items-center"}
                  style={{height: window.innerHeight - 200}}>

                Ахахах, пусто!


            </Card> :
                    <Row className="mt-2">
                        <Col>
                            <Card className={" justify-content-center align-items-center"}
                                  style={{display: "grid"}}
                            >



                                {
                                    basket.products.map((product) =>
                                        <div key={product.basket_id}>
                                            <ProductItem  device={product}/>
                                            <Button style={{marginTop: 5}}  onClick={() => removeFromBasket(product.basket_id)}>Убрать товар из корзины</Button>
                                        </div>
                                    )
                                }
                            </Card>
                        </Col>
                        <Col style={{width: 400}}>
                            <div style={{display: "block",
                                textAlign: "center",
                                width: 230}} >
                                <Card className={" justify-content-center align-items-center"}>

                                    <p>Итого: {basket.products.length} товаров на {basket.products.reduce((a,b)=>a+b.price, 0)} руб.</p>




                                </Card>

                                <Button style={{marginTop: 15}} onClick={() => history.push(CHECKOUT_ROUTE)}>Оформить заказ</Button>
                            </div>









                        </Col>
                    </Row>



            }
        </Container>
    );
};

export default observer(Basket);