import React, {useContext, useState} from 'react';
import {Button, Card, Container, Form} from "react-bootstrap";
import {Context} from "../index";
import {useHistory} from "react-router-dom";
import {ORDERS_ROUTE} from "../utils/const";
import {checkOut, getBasket, getMyOrders} from "../http/basketApi";
import {observer} from "mobx-react-lite";



const CheckOut = () => {
    const {basket} = useContext(Context)
    const {order} = useContext(Context)

    const [address, setAddress] = useState('')

    const history = useHistory()
    const checkedOut = () => {

        checkOut(address).then((data) => {
            getMyOrders().then(data => {

                order.setOrders(data)

            })
        })



        basket.setProducts([])



        history.push(ORDERS_ROUTE)

    }
    return (
        <Container className={"d-flex justify-content-center align-items-center"}  style={{height: window.innerHeight - 54}}>
            <Card  style={{width: 600}} className="p-5">
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Адрес доставки</Form.Label>
                        <Form.Control type="address" placeholder="Введите адрес"
                              onChange={(e) => setAddress(e.target.value)}/>
                        <Form.Text className="text-muted">
                            Укажите точный адрес, что бы мы могли доставить вам прямо до двери.
                        </Form.Text>
                    </Form.Group>



                    <Button variant="primary" onClick={() => checkedOut()}>
                        Заказать
                    </Button>
                </Form>
            </Card>
        </Container>
    );
};

export default observer(CheckOut);