import React, {useContext, useEffect} from 'react';
import { Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col"

import DeviceList from "../components/DeviceList";
import {observer} from "mobx-react-lite";
import {getProducts} from "../http/deviceAPI";
import {Context} from "../index";


const Shop = () => {




    return (
        <Container>
            <Row className="mt-2">
                <Col md={2}>

                </Col>
                <Col md={9}>

                    <DeviceList/>

                </Col>
            </Row>
        </Container>
    );
};

export default observer(Shop);
