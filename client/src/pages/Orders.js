import React, {useContext, useEffect, useState} from 'react';
import {
    Button,
    Card,
    Container,
    Dropdown,
    DropdownButton,
    FormControl,
    FormLabel,
    InputGroup,
    Modal,
    Row
} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import ProductItem from "../components/ProductItem";
import {Context} from "../index";
import {render} from "@testing-library/react";
import {Accordion} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {getProducts} from "../http/deviceAPI";
import {dropOrder} from "../http/basketApi";
import {$authHost} from "../http";

const Orders = () => {
    const {order} = useContext(Context)
    const {device} = useContext(Context)
    const {user} = useContext(Context)
    const [orderItem, setOrderItem] = useState({products: []})
    const [show, setShow] = useState(false);
    const [date, setDate] = useState();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    console.log(order)
    const cancelOrder = (id) => {
        alert("И поделом!")
        dropOrder(id).then(()=> order.setOrders(order.orders.filter((i) => i.id!==id)))

    }

    useEffect(() => {

        getProducts().then(data => {
            device.setDevices(data)

        })
    }, [device])

    if(  order.orders.length===0) return (
        <Container style={{padding: 40}}>
        <Card className={" justify-content-center align-items-center"}
              style={{height: window.innerHeight - 200}}>

            Ахахах, пусто!


        </Card>
            </Container>
    )

    return (
        <Container style={{padding: 40}}>
            <Accordion defaultActiveKey="0">

                {
                    order.orders.map((order) =>
                        <Accordion.Item key={order.id} eventKey={order.id} >
                            <Accordion.Header>Заказ #{order.id}</Accordion.Header>

                            <Accordion.Body>

                                {user.user.role===0 &&
                                <InputGroup className="mb-3">
                                    <DropdownButton
                                        variant="outline-secondary"
                                        title="Изменить статус заказа"
                                        id="input-group-dropdown-1"
                                    >
                                        <Dropdown.Item  value={"Created"} onClick={e => $authHost.put('api/v1/order/'+order.id, {status: "Created"}).then()}>Создано</Dropdown.Item>
                                        <Dropdown.Item  value={"Processing"} onClick={e => $authHost.put('api/v1/order/'+order.id, {status: "Processing"}).then()}>Собирается</Dropdown.Item>
                                        <Dropdown.Item  value={"Sent"} onClick={e => $authHost.put('api/v1/order/'+order.id, {status: "Sent"}).then()}>Отправлено</Dropdown.Item>
                                        <Dropdown.Item  value={"Delivered"} onClick={e => $authHost.put('api/v1/order/'+order.id, {status: "Delivered", paid: true}).then()}>Доставлено</Dropdown.Item>
                                    </DropdownButton>

                                </InputGroup>}
                                <Card.Body>
                                    <Row className="mt-2">
                                        <h2 style={{padding: "20px 0px 20px 0px"}}>Номер заказа: {order.id}</h2>
                                        <Col>
                                            <Card className={" justify-content-center align-items-center"}
                                                  style={{display: "grid"}}
                                            >
                                                {order.products.map((product)=>
                                                    <ProductItem key={product.id+Date.now()} device={product}/>
                                                )}



                                            </Card>
                                        </Col>
                                        <Col style={{width: 400}}>
                                            <div style={{display: "block",
                                                textAlign: "center",
                                                width: 230}} >

                                                <Row>

                                                    <div>Время отправления</div>

                                                    <FormLabel style={{borderStyle: "solid", padding: 20}}>{order.departure_time?order.departure_time:'Пока не отправлено'}</FormLabel>
                                                </Row>
                                                <Row>
                                                    <div style={{marginTop: 15}}>Время прибытия</div>
                                                    <FormLabel style={{borderStyle: "solid", padding: 20}}>{order.arrival_time}</FormLabel>

                                                </Row>
                                                <Row>
                                                    <div style={{marginTop: 15}}>Статус</div>
                                                    <FormLabel style={{borderStyle: "solid", padding: 20}}>{order.status}</FormLabel>

                                                </Row>
                                                <Row>
                                                    {console.log(order)}
                                                    <div style={{marginTop: 15}}>Адрес доставки</div>
                                                    <FormLabel style={{borderStyle: "solid", padding: 20}}>{order.delivery_address}</FormLabel>

                                                </Row>
                                                <Row>
                                                    <div style={{marginTop: 15}}>Статус оплаты</div>
                                                    <FormLabel style={{borderStyle: "solid", padding: 20, backgroundColor: "darkgreen", color: "white"}}>{order.paid?<>Оплачено</>:<>Не оплачено</>}</FormLabel>

                                                </Row>
                                            </div>
                                        </Col>


                                        { !order.paid && <Button style={{backgroundColor: "red", marginTop: 20}} onClick={() => cancelOrder(order.id)}>Отменить заказ</Button> }
                                    </Row>

                                </Card.Body>
                            </Accordion.Body>
                        </Accordion.Item>
                    )
                }


            </Accordion>












        </Container>
    );
};

export default observer(Orders);