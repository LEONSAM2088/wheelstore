import React, {useContext, useEffect, useState} from 'react';
import {Container, Spinner} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import {observer} from "mobx-react-lite";
import {useHistory, useParams} from 'react-router-dom'
import Comments from "../components/Comments";
import {ADDCOMMENT_ROUTE, ADDPRODUCT_ROUTE, COMMENTS_ROUTE, LOGIN_ROUTE} from "../utils/const";
import {Context} from "../index";
import {getCommentsForProduct, getProduct, getRating} from "../http/deviceAPI";
import {pushProduct, getBasket} from "../http/basketApi";

const DevicePage = () => {
    const {user} = useContext(Context)
    const {comment} = useContext(Context)
    const {basket} = useContext(Context)
    const [device, setDevice] = useState({info: [], img: "dfa773be-c623-41b5-91ba-595c38c4c409.jpg"})
    const {id} = useParams()
    const history = useHistory()
    const [loading, setLoading] = useState(true)
    const [rate, setRate] = useState(5)
    useEffect(async () => {
        const product = await getProduct(id)
        setDevice(product)
        getRating(id).then(data => {
            console.log(data)
            if(data.data.length>0)
                setRate(data.data.reduce((a,b) => a+b.rate , 0)/data.data.length)

        })
        getCommentsForProduct(id).then(data => {
            comment.setComments(data)

        }).finally(() => setLoading(false))
    }, [comment])
    if(loading) {
        return <Spinner animation={"grow"}/>
    }


     async function pushToBasket(id) {
         let is = false
         const baskP = await getBasket()

         const sos = baskP.filter(i => i.id == id)

         if (sos.length === 0) is = true


         pushProduct(id).then(r => {
             getBasket().then(r=> {

                 basket.setProducts(r)
             })
             alert("Добавлено в корзину")
         })

     }
    const editProduct = () => {
        const title = device.title
        const price = device.price
        const image = device.image
        history.push(ADDPRODUCT_ROUTE, {id, title, price, image})

    }
    return (
        <Container className="mt-3">
            <Row>
                <Col md={4}>
                    <Image width={300} height={300} src={device.image}/>
                </Col>
                <Col md={4}>
                    <Row className="d-flex flex-column align-items-center">
                        {user.user.role===0 &&  <Button variant={'outline-danger'} onClick={() => editProduct()}>Редактировать</Button> }

                        <h2>{device.title}</h2>
                        <div
                            className="d-flex align-items-center justify-content-center"
                            style={{background: `url('https://sokrovennik.ru/uploads/wysiwyg/images/blobid51.png') no-repeat center`, width:260, height:240, backgroundSize: 'cover', fontSize: 64}}
                        >
                            {rate.toFixed(1)}
                        </div>
                    </Row>
                </Col>
                <Col md={4}>
                    <Card
                        className="d-flex flex-column align-items-center justify-content-around"
                        style={{width: 300, height: 300, fontSize: 32, border: '5px solid lightgray'}}
                    >
                        <h3>От: {device.price} руб.</h3>
                        <Button variant={'outline-dark'} onClick={() => pushToBasket(id)}>Добавить в корзину</Button>
                    </Card>
                </Col>
            </Row>
            <Row className="d-flex flex-column m-3">

                {false&&device.info.map((info, index) =>
                    <Row key={info.id} style={{background: index % 2 === 0 ? 'lightgray' : 'transparent', padding: 10}}>
                        {info.title}: {info.description}
                    </Row>
                )}
            </Row>

            {user.LevelAccess<2 && <div style={{width: "100%", height: 40, marginBottom: 50}}>
                <Button style={{position: "absolute", right: 70}} onClick={()=>history.push(ADDCOMMENT_ROUTE+'/'+id)}>Оставаить отзыв</Button>
            </div>}
            {comment.comments.length>0 &&
            <Comments item={comment.comments[0]}/>}

            <Button style={{bottom: 0, width: "100%", marginBottom: 100}} onClick={()=>history.push(COMMENTS_ROUTE+'/'+id)}>Все отзывы</Button>
        </Container>
    );

};

export default observer(DevicePage);
