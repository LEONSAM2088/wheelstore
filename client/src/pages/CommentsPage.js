import React, {useContext, useEffect, useState} from 'react';
import {Container, Spinner} from "react-bootstrap";
import Comments from "../components/Comments";
import {Context} from "../index";
import {check, registrationTempUser} from "../http/userAPI";
import {getCommentsForProduct, getProducts} from "../http/deviceAPI";
import {useParams} from "react-router-dom";
import {observer} from "mobx-react-lite";

const CommentsPage = () => {
    const {comment} = useContext(Context)
    const [loading, setLoading] = useState(true)
    const {id} = useParams()
    useEffect(() => {

        getCommentsForProduct(id).then(data => {
            comment.setComments(data)

        }).finally(() => setLoading(false))



    }, [comment])
    if(loading) {
        return <Spinner animation={"grow"}/>
    }
    return (
        <Container style={{paddingTop: 20}}>
            <h1>Отзывы на товар Something Cool#2</h1>
            {
                comment.comments.map((comment) =>

                    <Comments key={comment.id} item={comment}/>
                )
            }



        </Container>
    );
};

export default observer(CommentsPage);