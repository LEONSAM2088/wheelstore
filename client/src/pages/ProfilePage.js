import React, {useContext, useState} from 'react';
import {Button, Card, Container, Form, FormLabel, Modal, Row} from "react-bootstrap";
import {NavLink, useHistory} from "react-router-dom";
import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/const";
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import {getUserInfo, refreshToken, registrationTempUser, setUserImage} from "../http/userAPI";
import {$authHost} from "../http";

const ProfilePage = () => {
    const {user} = useContext(Context)
    const {order} = useContext(Context)
    const {basket} = useContext(Context)
    const history = useHistory()
    const logOut = () => {
        user.setLevelAccess(2)
        user.setIsAuth(false)
        user.setUser({})
        order.setOrders([])
        basket.setProducts([])
        localStorage.removeItem('token')
        registrationTempUser().then(r => history.push(SHOP_ROUTE))

    }
    const [show, setShow] = useState(false);
    const [image, setImage] = useState();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleSave =  () => {
        setUserImage(user.user.id, image).then((data)=> {


            setTimeout(() => {
                refreshToken().then()

                user.user.image = data.data.user.image
            }, 1000);

        }).finally(() => setShow(false))

    }

    return (
        <Container className={"d-flex justify-content-center align-items-center"}
                   style={{height: window.innerHeight - 54}}
        >
            <Card style={{width: 600, height: 300}} className="p-5">
                <div style={{width: 200, display: "flex", marginLeft: 20}} >
                    <Button style={{width: 100, height: 100, borderRadius: 100}} onClick={handleShow}><img style={{borderRadius: 100, position: "relative", left: "-34px", top: -13}} src={user.user.image?user.user.image:'https://sun9-22.userapi.com/impg/zvYRDTJe3v51trKIOhHfJVKU3rBn_lajb0I2WA/SjX1VArVZYA.jpg?size=1280x823&quality=96&sign=d009c32b754cd5f2d774bfe9070b5ae8&type=album'} width={100} height={100} alt={"smth"}/></Button>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Изображение профиля</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group controlId="formFile" className="mb-3">
                                <Form.Label>Загрузи картинку, которую увидят все!</Form.Label>
                                <Form.Control type="file" onChange={(e) => {
                                    setImage(e.target.files[0])
                                }}/>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Закрыть
                            </Button>
                            <Button variant="primary" onClick={handleSave}>
                                Сохранить
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <div style={{width: 100}}>{user.user.name}</div>

                </div>

                <Form className="d-flex flex-column">

                    <Row className="d-flex justify-content-between mt-3 pl-3 pr-3">

                        <FormLabel>Почта: {user.user.email}</FormLabel>

                        <Button
                            className="align-self-end"
                            variant="outline-success"
                            onClick={() => logOut()}
                        >

                            Выйти
                        </Button>
                    </Row>

                </Form>
            </Card>


        </Container>
    );
};

export default observer(ProfilePage);