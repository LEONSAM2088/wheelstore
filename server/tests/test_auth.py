import requests
from string import digits, ascii_letters
from random import choices

url = 'http://localhost:5000/api/v1'
random_code_length = 6

def test_register_user_login_delete_user():
    random_code = choices(digits+ascii_letters, k=random_code_length)
    response = requests.post(f'{url}/auth/register', json=f'{{ name: Name{random_code}, '
                                                          f'email: email{random_code}@mail.ru,'
                                                          f'password: password{random_code} }}')
    print(response.text)
    assert response.status_code == 201

    response = requests.post(f'{url}/auth/login', json=f'{{ email: email{random_code},'
                                                       f'password: password{random_code}}}')
    assert response.status_code == 200
    response_body = response.json()
    assert 'access' in response_body
    assert 'refresh' in response_body
    assert 'user' in response_body
    user_id = response_body['user']['id']

    response = requests.delete(f'{url}/user/{user_id}')
    assert response.status_code == 201

    response = requests.post(f'{url}/auth/login', json=f'{{ email: email{random_code},'
                                                       f'password: password{random_code}}}')
    assert response.status_code == 401

    return True


def run_tests():
    test_register_user_login_delete_user()


if __name__ == '__main__':
    run_tests()


