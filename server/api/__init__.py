import os

from flask import Flask, send_from_directory
from api.blueprints import auth
from api.blueprints import allother
from api.database import db
from dotenv import load_dotenv
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from datetime import timedelta



def create_app(test_config=None):
    load_dotenv()

    app = Flask(__name__, instance_relative_config=True, static_url_path='')
    cors = CORS(app, supports_credentials=True)

    if test_config is None:
        app.config.from_mapping(
            SECRET_KEY=os.environ.get('SECRET_KEY'),
            SQLALCHEMY_DATABASE_URI=os.environ.get('SQLALCHEMY_DB_URI'),
            JWT_SECRET_KEY=os.environ.get('JWT_SECRET_KEY'),
            MAX_CONTENT_LENGTH=1024*1024*10,
            UPLOAD_EXTENSIONS=['.jpg', '.png', '.gif'],
            UPLOAD_PATH='static',
            JWT_ACCESS_TOKEN_EXPIRES=timedelta(minutes=30)

        )
    else:

        app.config.from_mapping(test_config)

    db.app = app
    db.init_app(app)

    JWTManager(app)

    db.create_all()
    app.register_blueprint(auth.auth)
    app.register_blueprint(allother.allother)

    @app.route('/static/<path:path>', methods=('GET',))
    def send_js(path):
        return send_from_directory('static', path)

    return app
