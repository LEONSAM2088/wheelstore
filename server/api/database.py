from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    name = db.Column(db.String(80), nullable=True)
    password = db.Column(db.Text, nullable=False)
    role = db.Column(db.Integer, nullable=False)
    image = db.Column(db.Text, nullable=True)

    def to_dict(self):
        return {'id': self.id, 'email': self.email, 'name': self.name, 'role': self.role, 'image': self.image}


class Order(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    status = db.Column(db.String(20), nullable=False, default='Created')
    departure_time = db.Column(db.DateTime, nullable=True)
    arrival_time = db.Column(db.DateTime, nullable=True)
    delivery_address = db.Column(db.Text, nullable=True)
    paid = db.Column(db.Boolean, nullable=False, default=False)

    # user = db.relationship('User', foreign_keys='Order.user_id')

    def to_dict(self):
        return {'id': self.id, 'user_id': self.user_id, 'status': self.status,
                'departure_time': self.departure_time.strftime('%d.%m.%Y'),
                'arrival_time': self.arrival_time.strftime('%d.%m.%Y'),
                'delivery_address': self.delivery_address, 'paid': self.paid}


class Product(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    title = db.Column(db.String(40), nullable=False)
    price = db.Column(db.Float, nullable=False)
    image = db.Column(db.Text, nullable=True)
    in_stock = db.Column(db.Boolean, nullable=False, default=True)

    def to_dict(self):
        return {'id': self.id, 'title': self.title, 'price': self.price, 'image': self.image, 'in_stock': self.in_stock}


class Comment(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    comment = db.Column(db.Text, nullable=False)
    unique_product_user = db.UniqueConstraint('product_id', 'user_id', name='unique_product_user')

    # user = db.relationship(User, foreign_keys=Comment.user_id)
    # product = db.relationship('Product', foreign_keys='Comment.product_id')
    def to_dict(self):
        user = User.query.filter_by(id=self.user_id).first()
        rate = Rate.query.filter_by(user_id=self.user_id, product_id=self.product_id).all()
        if len(rate) == 0:
            rate = None
        else:
            rate = rate[0].rate
        return {'id': self.id, 'product_id': self.product_id, 'user_id': self.user_id,
                'user_name': user.name, 'user_image': user.image, 'comment': self.comment, 'rate': rate}


class Rate(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    rate = db.Column(db.Integer, nullable=False)

    unique_product_user = db.UniqueConstraint('product_id', 'user_id', name='unique_product_user')

    # user = db.relationship('User', foreign_keys='Rate.user_id')
    # product = db.relationship('Product', foreign_keys='Rate.product_id')
    def to_dict(self):
        return {'id': self.id, 'product_id': self.product_id, 'user_id': self.user_id, 'rate': self.rate}


class ProductInfo(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id), nullable=False)
    title = db.Column(db.String(30), nullable=False)
    description = db.Column(db.Text, nullable=False)

    # product = db.relationship('Product', foreign_keys='ProductInfo.product_id')
    def to_dict(self):
        return {'id': self.id, 'product_id': self.product_id, 'title': self.title, 'description': self.description}


class ProductOrder(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id), nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey(Order.id), nullable=False)

    # product = db.relationship('Product', foreign_keys='ProductOrder.product_id')
    # order = db.relationship('Order', foreign_keys='ProductOrder.order_id')
    def to_dict(self):
        return {'id': self.id, 'product_id': self.product_id, 'user_id': self.order_id}


class BasketProduct(db.Model):
    id = db.Column(db.Integer, unique=True, autoincrement=True, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)

    # product = db.relationship('Product', foreign_keys='BasketProduct.product_id')
    # basket = db.relationship('User', foreign_keys='BasketProduct.user_id')
    def to_dict(self):
        return {'id': self.id, 'product_id': self.product_id, 'user_id': self.user_id}
