from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from ..database import Product, User, Rate, Comment, Order, ProductOrder, BasketProduct, db
from datetime import datetime, timedelta
from random import choices
from string import digits, ascii_letters
import os
from werkzeug.utils import secure_filename

allother = Blueprint('allother', __name__, url_prefix='/api/v1')


@allother.route('/products', methods=('GET', 'POST'))
@jwt_required(optional=True)
def products():
    if request.method == 'GET':
        return jsonify([x.to_dict() for x in Product.query.all()]), 200

    if request.method == 'POST':
        json_data = request.get_json(force=True)
        user = get_jwt_identity()
        if user is None:
            return jsonify({'message': 'You do not have enough rights to create new products.'}), 401  # Unauthorized

        if user['role'] != 0:
            return jsonify({'message': 'You do not have enough rights to create new products.'}), 401  # Unauthorized
        title = json_data.get('title')
        price = json_data.get('price')
        product = Product(title=title, price=price)
        db.session.add(product)
        db.session.flush()
        db.session.refresh(product)

        for uploaded_file in request.files.getlist('') + request.files.getlist('file'):
            if uploaded_file.filename != '':
                ext = os.path.splitext(uploaded_file.filename)[1].lower()
                new_filename = os.path.join('products', f'{product.id}{ext}')
                uploaded_file.save(
                    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'static', new_filename))
                product.image = request.host_url + 'static/' + new_filename.replace('\\', '/')
                break

        db.session.commit()
        return jsonify({'message': 'product added',
                        'product': product.to_dict()}), 201


@allother.route('/users', methods=('GET',))
@jwt_required()
def users():
    user = get_jwt_identity()
    if user['role'] != 0:
        return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized

    if request.method == 'GET':
        return jsonify([x.to_dict() for x in User.query.all()]), 200


@allother.route('/users/<user_id>', methods=('PUT', 'POST'))
@jwt_required()
def change_user(user_id):
    user_id = int(user_id)
    user = get_jwt_identity()
    if user['role'] != 0 and user['id'] != user_id:
        return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized

    json_data = request.get_json()
    user_obj = User.query.filter_by(id=user_id).first()
    if json_data:
        if 'email' in json_data and user['role'] != 0:
            return jsonify({'message': 'You do not have enough rights to change email or password'}), 401
        if 'name' in json_data:
            user_obj.name = json_data.get('name')
        if user['role'] == 0:
            if 'email' in json_data:
                user_obj.email = json_data.get('email')
            if 'password' in json_data:
                user_obj.password = json_data.get('password')

    for uploaded_file in request.files.getlist('') + request.files.getlist('file'):
        if uploaded_file.filename != '':
            ext = os.path.splitext(uploaded_file.filename)[1].lower()
            fn = ''.join(choices(digits + ascii_letters, k=10))
            new_filename = os.path.join('users', f'{fn}{ext}')
            uploaded_file.save(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'static', new_filename))
            user_obj.image = request.host_url + 'static/' + new_filename.replace('\\', '/')
            break

    db.session.commit()
    return jsonify({'message': 'User was updated successfully', 'user': user_obj.to_dict()}), 200


@allother.route('/user/<user_id>', methods=('DELETE',))
@jwt_required()
def delete_user(user_id: int):
    user_id = int(user_id)
    user = get_jwt_identity()
    if user['role'] != 0:
        return jsonify({'message': 'You do not have enough rights to create new products.'}), 401  # Unauthorized

    User.query.filter_by(id=user_id).delete()
    db.session.commit()
    return jsonify({'message': 'User was deleted successfully'}), 201


@allother.route('/products/<product_id>', methods=('GET', 'DELETE', 'PUT'))
@jwt_required(optional=True)
def get_product(product_id: int):
    product_id = int(product_id)
    if request.method == 'GET':
        product = Product.query.filter_by(id=product_id).first()
        if product is None:
            return jsonify({'message': f'No product with id {id}'}), 400
        return jsonify(product.to_dict()), 200
    if request.method == 'DELETE':
        user = get_jwt_identity()
        if user is None or user['role'] != 0:
            return jsonify({'message': f'You do not have enough rights'}), 401
        Product.query.filter_by(id=product_id).delete()
        db.session.commit()
        return jsonify({'message': 'Product was deleted successfully'}), 201
    if request.method == 'PUT':
        json_data = request.get_json()
        user = get_jwt_identity()
        if user is None or user['role'] != 0:
            return jsonify({'message': f'You do not have enough rights'}), 401
        product = Product.query.filter_by(id=product_id).first()
        if json_data:
            if 'title' in json_data:
                product.title = json_data.get('title')
            if 'price' in json_data:
                product.price = float(json_data.get('price'))
            if 'in_stock' in json_data:
                product.in_stock = json_data.get('in_stock').lower() in ('true', '1', 'yes')

        for uploaded_file in request.files.getlist('') + request.files.getlist('file'):
            if uploaded_file.filename != '':
                ext = os.path.splitext(uploaded_file.filename)[1].lower()
                new_filename = os.path.join('products', f'{product_id}{ext}')
                uploaded_file.save(
                    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'static', new_filename))
                product.image = request.host_url + 'static/' + new_filename.replace('\\', '/')
                break

        db.session.commit()
        return jsonify({'message': 'Product was updated successfully', 'product': product.to_dict()}), 200


@allother.route('/rating/<product_id>', methods=('GET', 'POST',))
@jwt_required(optional=True)
def get_product_rates(product_id: int):
    product_id = int(product_id)
    if request.method == 'GET':
        json_data = request.get_json()
        rates = Rate.query.filter_by(product_id=product_id).all()
        if json_data is not None and json_data.get('user_id') is not None:
            rate = Rate.query.filter_by(product_id=product_id, user_id=json_data.get('user_id')).first()
            return jsonify(
                None if rate is None else {'id': rate.id, 'user_id': rate.user_id, 'product_id': rate.product_id,
                                           'rate': rate.rate}), 200
        return jsonify([rate.to_dict() for rate in rates]), 200

    if request.method == 'POST':
        user = get_jwt_identity()
        json_data = request.get_json()
        num_rate = json_data.get('rate')
        rate = Rate(product_id=product_id, user_id=user['id'], rate=num_rate if str(num_rate).isnumeric() else None)
        db.session.add(rate)
        db.session.commit()
        return jsonify({'message': 'Rate registered', 'rate': rate.to_dict()})


@allother.route('/rate/<rate_id>', methods=('DELETE', 'PUT'))
@jwt_required(optional=True)
def delete_rate(rate_id: int):
    rate_id = int(rate_id)
    user = get_jwt_identity()
    rate = Rate.query.filter_by(id=rate_id).first()
    if rate is None:
        return jsonify({'message': f'There is no rate with id {rate_id}'}), 400
    if user is None or not (user['id'] == rate.user_id or user['role'] == 0):
        return jsonify({'message': f'You do not have enough rights'}), 401

    if request.method == 'DELETE':
        Rate.query.filter_by(id=rate_id).delete()
        db.session.commit()
        return jsonify({'rate': 'Comment deleted'}), 201

    if request.method == 'PUT':
        json_data = request.get_json()
        rate = Rate.query.filter_by(id=rate_id).first()
        if 'rate' in json_data:
            rate.rate = int(json_data.get('rate'))
        db.session.commit()
        return jsonify({'message': 'Comment deleted'}), 201


@allother.route('/comments/<product_id>', methods=('GET', 'POST'))
@jwt_required(optional=True)
def get_product_comments(product_id: int):
    product_id = int(product_id)
    if request.method == 'GET':
        comments = Comment.query.filter_by(product_id=product_id).all()
        return jsonify(
            [comment.to_dict() for comment in comments]), 200

    if request.method == 'POST':
        user = get_jwt_identity()
        json_data = request.get_json()
        text = json_data.get('text')
        if user is None:
            return jsonify({'message': 'You are not registered'}), 401
        if text is None:
            return jsonify({'message': 'There is no comment'}), 400
        comment = Comment(product_id=product_id, user_id=user['id'], comment=text)
        db.session.add(comment)
        db.session.commit()
        return jsonify({'message': 'Comment registered', 'comment': comment.to_dict()}), 201


@allother.route('/comment/<comment_id>', methods=('DELETE', 'PUT'))
@jwt_required(optional=True)
def delete_comment(comment_id: int):
    comment_id = int(comment_id)
    user = get_jwt_identity()
    comment = Comment.query.filter_by(id=comment_id).first()
    if comment is None:
        return jsonify({'message': f'There is no comment with id {comment_id}'}), 400
    if user is None or not (user['id'] == comment.user_id or user['role'] == 0):
        return jsonify({'message': f'You do not have enough rights'}), 401

    if request.method == 'DELETE':
        Comment.query.filter_by(id=comment_id).delete()
        db.session.commit()
        return jsonify({'message': 'Comment deleted'}), 201

    if request.method == 'PUT':
        json_data = request.get_json(force=True)
        comment = Comment.query.filter_by(id=comment_id).first()
        if 'text' in json_data:
            comment.comment = json_data.get('text')
        db.session.commit()
        return jsonify({'message': 'Comment deleted'}), 201


@allother.route('/orders/<customer_id>', methods=('GET',))
@jwt_required()
def get_user_orders(customer_id):
    user = get_jwt_identity()

    if customer_id == 'all':
        if user['role'] != 0:
            return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized

        orders = Order.query.all()
        products_in_orders = [[Product.query.filter_by(id=x.product_id).first().to_dict()
                               for x in ProductOrder.query.filter_by(order_id=order.id)] for order in orders]
        return jsonify([dict(**order.to_dict(), products=products_in_order)
                        for order, products_in_order in zip(orders, products_in_orders)]), 200

    customer_id = int(customer_id)
    if user['role'] != 0:
        return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized

    if request.method == 'GET':
        orders = Order.query.filter_by(user_id=customer_id).all()
        products_in_orders = [[Product.query.filter_by(id=x.product_id).first().to_dict()
                               for x in ProductOrder.query.filter_by(order_id=order.id)] for order in orders]
        return jsonify([dict(**order.to_dict(), products=products_in_order)
                        for order, products_in_order in zip(orders, products_in_orders)]), 200


@allother.route('/order/<order_id>', methods=('GET', 'PUT', 'DELETE'))
@jwt_required()
def get_order(order_id: int):
    order_id = int(order_id)
    user = get_jwt_identity()
    order = Order.query.filter_by(id=order_id).first()

    if user is None or not (user['id'] == order.user_id or user['role'] == 0):
        return jsonify({'message': f'You do not have enough rights'}), 401

    if request.method == 'GET':
        products_in_order = [Product.query.filter_by(id=x.product_id).first().to_dict()
                             for x in ProductOrder.query.filter_by(order_id=order.id)]
        return jsonify(dict(**order.to_dict(), products=products_in_order)), 200

    if request.method == 'PUT':
        if user['role'] != 0:
            return jsonify({'message': f'You do not have enough rights'}), 401
        json_data = request.get_json(force=True)
        if 'status' in json_data:
            new_status = json_data.get('status')
            order.status = new_status
            if new_status == 'Sent':
                order.departure_time = datetime.now()
            if new_status == 'Delivered':
                order.arrival_time = datetime.now()
                order.paid = True

        if 'delivery_address' in json_data:
            order.delivery_address = json_data.get('delivery_address')
        if 'paid' in json_data:
            order.paid = bool(json_data.get('paid'))
        if 'departure_days' in json_data:
            order.departure_time = datetime.now() + timedelta(days=int(json_data.get('departure_days')))
        if 'arrival_days' in json_data:
            order.arrival_time = datetime.now() + timedelta(days=int(json_data.get('arrival_days')))
        db.session.commit()
        return jsonify({'message': 'Order info updated successfully', 'order': order.to_dict()}), 200

    if request.method == 'DELETE':
        Order.query.filter_by(id=order_id).delete()
        ProductOrder.query.filter_by(order_id=order_id).delete()
        db.session.commit()
        return jsonify({'message': 'Order deleted successfully'}), 200


@allother.route('/orders', methods=('GET', 'POST'))
@jwt_required()
def get_orders():
    user = get_jwt_identity()

    if request.method == 'GET':
        orders = Order.query.filter_by(user_id=user['id']).all()
        products_in_orders = [[Product.query.filter_by(id=x.product_id).first().to_dict()
                               for x in ProductOrder.query.filter_by(order_id=order.id)] for
                              order in orders]
        return jsonify([dict(**order.to_dict(), products=products_in_order)
                        for order, products_in_order in zip(orders, products_in_orders)]), 200

    if request.method == 'POST':
        json_data = request.get_json(force=True)
        order = Order(user_id=user['id'], delivery_address=json_data.get('delivery_address'))
        db.session.add(order)
        db.session.flush()
        db.session.refresh(order)

        order_products = [ProductOrder(product_id=x.product_id, order_id=order.id)
                          for x in BasketProduct.query.filter_by(user_id=user['id']).all()]
        for x in order_products:
            db.session.add(x)
        BasketProduct.query.filter_by(user_id=user['id']).delete()
        db.session.commit()
        products_in_order = [x.to_dict() for x in order_products]
        return jsonify({'message': 'Order created', 'order':
            dict(**order.to_dict(), products=[x.product_id for x in order_products])})


@allother.route('/basket', methods=('GET', 'POST'))
@jwt_required()
def get_basket():
    user = get_jwt_identity()

    if request.method == 'GET':
        basket_products = BasketProduct.query.filter_by(user_id=user['id']).all()
        products_in_basket = [{**(Product.query.filter_by(id=x.product_id).first().to_dict()), 'basket_id': x.id} for x
                              in basket_products]
        return jsonify(products_in_basket), 200

    if request.method == 'POST':
        json_data = request.get_json(force=True)
        basket_product = BasketProduct(product_id=json_data.get('product_id'), user_id=user['id'])
        db.session.add(basket_product)
        db.session.commit()
        return jsonify({'message': 'Product added to cart', 'product_id': basket_product.product_id}), 200


@allother.route('/basket/<customer_id>', methods=('GET', 'POST', 'DELETE'))
@jwt_required()
def get_user_basket(customer_id: int):
    customer_id = int(customer_id)
    user = get_jwt_identity()

    if request.method == 'GET':
        if user['role'] != 0 and user['id'] != int(customer_id):
            return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized
        basket_products = BasketProduct.query.filter_by(user_id=customer_id).all()
        products_in_basket = [{**(Product.query.filter_by(id=x.product_id).first().to_dict()), 'basket_id': x.id} for x
                              in basket_products]
        return jsonify(products_in_basket), 200

    if request.method == 'POST':
        json_data = request.get_json(force=True)
        basket_product = BasketProduct(product_id=json_data.get('product_id'), user_id=customer_id)
        db.session.add(basket_product)
        db.session.commit()
        return jsonify({'message': 'Product added to cart',
                        'product_id': basket_product.product_id, 'user_id': customer_id}), 201

    if request.method == 'DELETE':
        if user['role'] != 0 and BasketProduct.query.filter_by(id=customer_id).first().user_id != user['id']:
            return jsonify({'message': 'You do not have enough rights.'}), 401  # Unauthorized
        BasketProduct.query.filter_by(id=customer_id).delete()
        db.session.commit()
        return jsonify({'message': 'Product deleted from the cart'}), 201

