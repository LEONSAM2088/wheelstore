from flask import Blueprint, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from api.database import User, db
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity, \
    set_refresh_cookies, unset_refresh_cookies
from random import choices
from string import digits, ascii_letters
from ..database import BasketProduct

auth = Blueprint('auth', __name__, url_prefix='/api/v1/auth')


@auth.route('/register', methods=('POST',))
@jwt_required(optional=True)
def register():
    identity = get_jwt_identity()
    json_data = request.get_json(force=True)
    email = json_data.get('email')
    name = json_data.get('name')
    password = json_data.get('password')
    role = json_data.get('role')

    if str(role) != '2':
        if len(password) < 6:
            return jsonify({'error': "Password is too short"}), 400  # Bad Request

        if len(name) < 2:
            return jsonify({'error': "Name is too short"}), 400  # Bad Request

        if not name.replace(' ', '').isalnum():
            return jsonify({'error': "Name should be alphanumeric"}), 400  # Bad Request

        if not validators.email(email):
            return jsonify({'error': "Email is not valid"}), 400  # Bad Request

        if User.query.filter_by(email=email).first() is not None:
            return jsonify({'error': "Email is occupied"}), 409  # Conflict

        pwd_hash = generate_password_hash(password)

        max_allowed_role = 1  # change to 0 to be able to register an admin
        if role is not None and str(role).isnumeric():
            role = max(int(role), max_allowed_role)
        else:
            role = max_allowed_role
        user = User(name=name, password=pwd_hash, email=email, role=role)

    else:
        while True:
            name = 'tmp_' + ''.join(choices(digits + ascii_letters, k=10))
            if len(User.query.filter_by(email=name).all())==0:
                break
        user = User(name=name, email=name, password=name, role=2)

    db.session.add(user)
    db.session.flush()
    db.session.refresh(user)
    if identity and identity['role'] == 2:  # move products from guest to new user
        basket_products = BasketProduct.query.filter_by(user_id=identity['id']).all()
        for basket_product in basket_products:
            basket_product.user_id = user.id
        User.query.filter_by(id=identity['id']).delete()
    db.session.commit()

    access = create_access_token(user.to_dict())
    refresh = create_refresh_token(user.to_dict())
    response = jsonify({'message': 'User created',
                        'user': user.to_dict(),
                        'access': access})  # Created

    set_refresh_cookies(response, refresh)
    return response, 201


@auth.route('/login', methods=('POST',))
@jwt_required(optional=True)
def login():
    identity = get_jwt_identity()
    json_data = request.get_json(force=True)
    email = json_data.get('email')
    password = json_data.get('password')

    user = User.query.filter_by(email=email).first()

    if user:
        is_pass_correct = check_password_hash(user.password, password)

        if is_pass_correct:
            refresh = create_refresh_token(identity=user.to_dict())
            access = create_access_token(identity=user.to_dict())

            if identity and identity['role'] == 2:  # move products from guest to new user
                basket_products = BasketProduct.query.filter_by(user_id=identity['id']).all()
                for basket_product in basket_products:
                    basket_product.user_id = user.id
                User.query.filter_by(id=identity['id']).delete()
                db.session.commit()

            response = jsonify({'user': {'access': access, 'user': user.to_dict()}})
            set_refresh_cookies(response, refresh)
            return response, 200  # OK

    return jsonify({'error': 'Wrong credentials'}), 401  # Unauthorized


@auth.route('/me', methods=('GET',))
@jwt_required()
def me():
    user = get_jwt_identity()
    return jsonify(user), 200


@auth.route('/token/refresh', methods=('GET',))
@jwt_required(refresh=True, locations=['cookies'])
def refresh_users_token():

    identity = get_jwt_identity()
    user = User.query.filter_by(id=identity['id']).first()
    access = create_access_token(identity=user.to_dict())
    refresh = create_refresh_token(identity=user.to_dict())
    response = jsonify({'access': access})
    set_refresh_cookies(response, refresh)
    return response, 200


@auth.route('/check', methods=('GET',))
@jwt_required(optional=True)
def check():
    identity = get_jwt_identity()
    if identity is None:
        return jsonify({'message': 'User is not authorized', 'authorized': False}), 400  # Bad request
    return jsonify({'message': 'User is authorized', 'authorized': True}), 200


@auth.route('/logout', methods=('GET',))
@jwt_required(refresh=True)
def logout():
    response = jsonify({'message': 'Logged out successfully'})
    unset_refresh_cookies(response)
    return response, 200
